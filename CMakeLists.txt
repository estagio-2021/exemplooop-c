cmake_minimum_required(VERSION 3.20)
project(Emcapsulamento C)

set(CMAKE_C_STANDARD 99)

add_executable(Emcapsulamento main.c point/point.h point/point.c namedPoint/namedPoint.c namedPoint/namedPoint.h)
