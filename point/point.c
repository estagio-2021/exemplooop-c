//
// Created by julionovaes on 25/10/2021.
//

#include "point.h"
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

struct Point{

    double x,y;
};

struct Point * create(double x, double y){
    struct Point * P = malloc(sizeof(struct Point));
    P->x = x;
    P->y = y;

    return P;
}

double distance (struct Point *p1, struct Point *p2){

    double dx = p1->x - p2 -> x;
    double dy = p1->y - p2 -> y;

    return sqrt(dx*dx+dy*dy);
}

