//
// Created by julionovaes on 25/10/2021.
//

#ifndef EMCAPSULAMENTO_POINT_H
#define EMCAPSULAMENTO_POINT_H

struct Point;

struct Point * create(double x, double y);

double distance (struct Point *p1, struct Point *p2);


#endif //EMCAPSULAMENTO_POINT_H
