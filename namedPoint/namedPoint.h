//
// Created by julionovaes on 25/10/2021.
//

#ifndef EMCAPSULAMENTO_NAMEDPOINT_H
#define EMCAPSULAMENTO_NAMEDPOINT_H
struct namedPoint;
struct namedPoint * createN(double x, double y,char * name);
char * getName(struct namedPoint * np);
void setName(struct namedPoint *np, char * name);


#endif //EMCAPSULAMENTO_NAMEDPOINT_H
