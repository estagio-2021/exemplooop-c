//
// Created by julionovaes on 25/10/2021.
//
#include <stdlib.h>
#include "namedPoint.h"

struct namedPoint{

    double x,y;
    char * name;
};

struct namedPoint * createN(double x, double y,char * name){

    struct namedPoint * np = malloc(sizeof (struct namedPoint));

    np->name = name;
    np->y = y;
    np->x = x;

    return np;
}
char * getName(struct namedPoint * np){

    return np->name;

}
void setName(struct namedPoint *np, char * name){

    np->name = name;

}

