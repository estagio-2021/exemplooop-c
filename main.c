#include <stdio.h>
#include "point/point.h"
#include "namedPoint/namedPoint.h"

int main() {

    struct Point * p = create(10, 12);
    struct Point * p1 = create(8, 50);

    printf("distancia point %f\n", distance(p,p1));


    struct namedPoint* origin = createN(0.0,0.0,"origin");
    struct namedPoint* upperRight = createN(1.0,1.0,"upperRihgt");

    printf("distancia namedPoint = %f",distance((struct Point*) origin,(struct Point*) upperRight));

}
